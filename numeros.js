// Forma rustica
function solucionUno(arr, cantidadIndicada) {
  // se define variable para el retorno de la funcion
  var resultado = false
  // definicion de sentencia label para la detencion de ciclos añidados
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/label
  loop1:
  for (var i = arr.length - 1; i >= 0; i--) {
    // definicion de sentencia label para la detencion de ciclos añidados
    loop2:
    for (var j = arr.length - 1; j >= 0; j--) {
      // salto del ciclo si es el mismo elemento
      if (arr[i] == arr[j]) {
        continue;
      }
      // detencion del ciclo si la condicion se cumple
      if (arr[j] + arr[i] == cantidadIndicada) {
        // asignar true a la variable de retorno de la funcion
        resultado = true;
        // detencion del ciclos
        break loop1;
      }
    }
  }
  return resultado;
}

// Forma mas agil
function solucionDos(arrDos, cantidadIndicada) {
  // se define variable para el retorno de la funcion
  var resultado = false
  // definicion de sentencia label para la detencion de ciclos añidados
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/label
  loop1:
  // primer ciclo para recorrer el arreglo de forma inversa
  for (var i = arrDos.length - 1; i >= 0; i--) {
    // definicion de sentencia label para la detencion de ciclos añidados
    loop2:
    // segundo ciclo para recorrer el mismo arreglo de forma inversa y comparar los demas elementos
    for (var j = arrDos.length - 1; j >= 0; j--) {
      // salto del ciclo si es el mismo elemento
      if (arrDos[i] == arrDos[j]) {
        continue;
      }
      // detencion del ciclo si la condicion se cumple
      if (arrDos[j] + arrDos[i] == cantidadIndicada) {
        // asignar true a la variable de retorno de la funcion
        resultado = true;
        // detencion del ciclos
        break loop1;
      }
    }
    // se borra el elemento actual del arreglo para minimizar las posibilidades
    arrDos.splice(i, 1);
  }
  return resultado;
}

var array  = [1, 2, 3, 6];
var array2 = [5, 2, 3, 2];
var array3 = [4, 7, 1, 6];
var array4 = [9, 8, -3, 6];
var array5 = [-1, 2, 10, 3];

console.log(solucionUno(array, 9));  // => true
console.log(solucionUno(array2, 9)); // => false
console.log(solucionUno(array3, 9)); // => false
console.log(solucionUno(array4, 9)); // => false
console.log(solucionUno(array5, 9)); // => true

console.log(solucionDos(array, 9));  // => true
console.log(solucionDos(array2, 9)); // => false
console.log(solucionDos(array3, 9)); // => false
console.log(solucionDos(array4, 9)); // => false
console.log(solucionDos(array5, 9)); // => true