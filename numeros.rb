# Forma rustica
def solucion_uno(array, cantidad_indicada)
  # se define el captador de detencion del ciclo
  catch(:result) do
    # primer ciclo para recorrer el arreglo
    # se definen dos variables:
    # 'e' el elmento actual del arreglo
    # 'i' posicion del elmento actual del arreglo
    array.each_with_index do |e, i|
      # segundo ciclo para recorrer el mismo arreglo y comparar los demas elementos
      # se definen dos variables:
      # 'v' el elmento actual del arreglo
      # 'ii' posicion del elmento actual del arreglo
      array.each_with_index do |v, ii|
        # salto del ciclo si es el mismo elemento
        next if array[0] == array[ii]
        # detencion de ciclos si la condicion se cumple
        throw(:result, true) if e + v == cantidad_indicada
      end
    end
    # si se cumplen los dos ciclos y no se cumple con la condicion de suma se retorna un false
    false
  end
end

# Forma mas agil
def solucion_dos(array, cantidad_indicada)
  # se define el captador de detencion del ciclo
  catch(:result) do
    # primer ciclo para recorrer el arreglo
    # se definen dos variables:
    # 'e' el elmento actual del arreglo
    # 'i' posicion del elmento actual del arreglo
    array.each_with_index do |e, i|
      # segundo ciclo para recorrer el mismo arreglo y comparar los demas elementos
      # se definen dos variables:
      # 'v' el elmento actual del arreglo
      # 'ii' posicion del elmento actual del arreglo
      array.each_with_index do |v, ii|
        # salto del ciclo si es el mismo elemento
        next if array[0] == array[ii]
        # detencion de ciclos si la condicion se cumple
        throw(:result, true) if e + v == cantidad_indicada
      end
      # se borra el elemento actual del arreglo para minimizar las posibilidades
      array = array.drop(1)
    end
    # si se cumplen los dos ciclos y no se cumple con la condicion de suma se retorna un false
    false
  end
end

array  = [1, 2, 3, 6]
array2 = [5, 2, 3, 2]
array3 = [4, 7, 1, 6]
array4 = [9, 8, -3, 6]
array5 = [-1, 2, 10, 3]

puts solucion_uno(array, 9)  # => true
puts solucion_uno(array2, 9) # => false
puts solucion_uno(array3, 9) # => false
puts solucion_uno(array4, 9) # => false
puts solucion_uno(array5, 9) # => true

puts solucion_dos(array, 9)  # => true
puts solucion_dos(array2, 9) # => false
puts solucion_dos(array3, 9) # => false
puts solucion_dos(array4, 9) # => false
puts solucion_dos(array5, 9) # => true
